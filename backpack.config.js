module.exports = {
  webpack: (config, options, webpack) => {
    // Perform customizations to config
    // Important: return the modified config

    // changes the name of the entry point from index -> main.js
    config.entry.main = ["./services/main1.ts"];

    config.resolve.extensions = ["js", "json", "ts"];
    config.module.rules = [
      ...config.module.rules,
      { test: /\.tsx?$/, loader: "ts-loader" }
    ];
    return config;
  }
};
